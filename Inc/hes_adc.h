#ifndef __ADC_H_
#define __ADC_H_
#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f3xx_hal.h"
#include "cmsis_os2.h"
#include <stdint.h>

//todo this other may be changed
enum hes_Adc_Sensors_e
{
    ADC_EBATT_VOL_hes_adc = 0,
    ADC_IBATT_VOL_hes_adc,
    ADC_FUELCELL_VOL_hes_adc,
    ADC_FUELCELL_CUR_hes_adc,
    ADC_PRESSURE_1_hes_adc,
    ADC_PRESSURE_2_hes_adc,
    ADC_PT1K_TEMP_1_hes_adc,
    ADC_PT1K_TEMP_2_hes_adc,
    NUM_ADC_SENSORS_hes_adc,
};
enum hes_Adc_Channels_e
{
    ADC_CHANNEL1_hes_adc = 0,
    ADC_CHANNEL2_hes_adc,
    ADC_CHANNEL3_hes_adc,
    ADC_CHANNEL4_hes_adC,
    ADC_CHANNEL5_hes_adc,
    ADC_CHANNEL6_hes_adc,
    ADC_CHANNEL7_hes_adc,
    ADC_CHANNEL8_hes_adc,
    NUM_ADC_CHANNELS_hes_adc,
};

void vADCInit(void);
int iADC_read(int sensorID, u16 *rawValue);

#ifdef __cplusplus
}
#endif
#endif /* ADC_H_ */
