#ifndef _HES_PINMAP_H_
#define _HES_PINMAP_H_
#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f3xx_hal.h"
#include "cmsis_os2.h"
#include <stdint.h>

//Define Output
#define HIGH (1)
#define LOW (0)

#define PINDIRECTION_OUTPUT (0)
#define PINDIRECTION_INPUT (1)

#define DB_LED_GREEN_Out_0 IO1_OUT_0 // PB.8
#define DB_LED_GREEN_Out_1 IO1_OUT_1
#define INOUT1 (PINDIRECTION_OUTPUT)

#define FC_SHORTCIRCUIT_Out_0 IO2_OUT_0 // PC.11
#define FC_SHORTCIRCUIT_Out_1 IO2_OUT_1
#define INOUT2 (PINDIRECTION_OUTPUT)

#define FC_LOADONOFF_Out_0 IO3_OUT_0 // PC.12
#define FC_LOADONOFF_Out_1 IO3_OUT_1
#define INOUT3 (PINDIRECTION_OUTPUT)

#define PWR_BUTTON_ONOFF_In IO4_IN
#define INOUT4 (PINDIRECTION_INPUT)

#define PWR_LATCH_Out_0 IO5_OUT_0 // PC.9
#define PWR_LATCH_Out_1 IO5_OUT_1
#define INOUT5 (PINDIRECTION_OUTPUT)

#define LCD_CS_Out_0 IO6_OUT_0 // PB.12
#define LCD_CS_Out_1 IO6_OUT_1
#define INOUT6 (PINDIRECTION_OUTPUT)

#define LCD_RST_Out_0 IO7_OUT_0 // PC.6
#define LCD_RST_Out_1 IO7_OUT_1
#define INOUT7 (PINDIRECTION_OUTPUT)

#define LCD_DC_Out_0 IO8_OUT_0 // PC.7
#define LCD_DC_Out_1 IO8_OUT_1
#define INOUT8 (PINDIRECTION_OUTPUT)

#define SupplyValve_Out_1 IO9_OUT_0 // PA.8 Reverse on off
#define SupplyValve_Out_0 IO9_OUT_1
#define INOUT9 (PINDIRECTION_OUTPUT)

#define PurgeValve_Out_0 IO10_OUT_0 // PA.9
#define PurgeValve_Out_1 IO10_OUT_1
#define INOUT10 (PINDIRECTION_OUTPUT)

#define WaterPump_Out_0 IO11_OUT_0 // PA.7
#define WaterPump_Out_1 IO11_OUT_1
#define INOUT11 (PINDIRECTION_OUTPUT)

#define WaterValve_Out_0 IO12_OUT_0 // PA.10
#define WaterValve_Out_1 IO12_OUT_1
#define INOUT12 (PINDIRECTION_OUTPUT)

#define ADS1118_CS_Out_0 IO13_OUT_0 // PA.15
#define ADS1118_CS_Out_1 IO13_OUT_1
#define INOUT13 (PINDIRECTION_OUTPUT)

#define CHARGER_EN_Out_0 IO14_OUT_0 // PC.10
#define CHARGER_EN_Out_1 IO14_OUT_1
#define INOUT14 (PINDIRECTION_OUTPUT)

#define EFUSE_EN_In_15 IO15_IN
#define INOUT15 (PINDIRECTION_INPUT)

#define WATER_LVL_HIGH IO16_IN
#define INOUT16 (PINDIRECTION_INPUT)

#define WATER_LVL_LOW IO17_IN
#define INOUT17 (PINDIRECTION_INPUT)

#define IO1_OUT_0 GPIO_ResetBits(GPIOB, GPIO_Pin_8) // PB.8
#define IO1_OUT_1 GPIO_SetBits(GPIOB, GPIO_Pin_8)
#define IO1_IN (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_8))

#define IO2_OUT_0 GPIO_ResetBits(GPIOC, GPIO_Pin_11) // PC.11
#define IO2_OUT_1 GPIO_SetBits(GPIOC, GPIO_Pin_11)
#define IO2_IN (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_11))

#define IO3_OUT_0 GPIO_ResetBits(GPIOC, GPIO_Pin_12) // PC.12
#define IO3_OUT_1 GPIO_SetBits(GPIOC, GPIO_Pin_12)
#define IO3_IN (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_12))

#define IO4_OUT_0 GPIO_ResetBits(GPIOC, GPIO_Pin_8) // PC.8
#define IO4_OUT_1 GPIO_SetBits(GPIOC, GPIO_Pin_8)
#define IO4_IN (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_8))

#define IO5_OUT_0 GPIO_ResetBits(GPIOC, GPIO_Pin_9) // PC.9
#define IO5_OUT_1 GPIO_SetBits(GPIOC, GPIO_Pin_9)
#define IO5_IN (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_9))

#define IO6_OUT_0 GPIO_ResetBits(GPIOB, GPIO_Pin_12) // PB.12
#define IO6_OUT_1 GPIO_SetBits(GPIOB, GPIO_Pin_12)
#define IO6_IN GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_12)

#define IO7_OUT_0 GPIO_ResetBits(GPIOC, GPIO_Pin_6) // PC.6
#define IO7_OUT_1 GPIO_SetBits(GPIOC, GPIO_Pin_6)
#define IO7_IN GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_6)

#define IO8_OUT_0 GPIO_ResetBits(GPIOC, GPIO_Pin_7) // PC.7
#define IO8_OUT_1 GPIO_SetBits(GPIOC, GPIO_Pin_7)
#define IO8_IN GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_7)

#define IO9_OUT_0 GPIO_ResetBits(GPIOA, GPIO_Pin_8) // PA.8
#define IO9_OUT_1 GPIO_SetBits(GPIOA, GPIO_Pin_8)
#define IO9_IN GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_8)

#define IO10_OUT_0 GPIO_ResetBits(GPIOA, GPIO_Pin_9) // PA.9
#define IO10_OUT_1 GPIO_SetBits(GPIOA, GPIO_Pin_9)
#define IO10_IN GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_9)

#define IO11_OUT_0 GPIO_ResetBits(GPIOA, GPIO_Pin_7) // PA.7
#define IO11_OUT_1 GPIO_SetBits(GPIOA, GPIO_Pin_7)
#define IO11_IN GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_7)

#define IO12_OUT_0 GPIO_ResetBits(GPIOA, GPIO_Pin_10) // PA.10
#define IO12_OUT_1 GPIO_SetBits(GPIOA, GPIO_Pin_10)
#define IO12_IN GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_10)

#define IO13_OUT_0 GPIO_ResetBits(GPIOA, GPIO_Pin_15) // PA.15
#define IO13_OUT_1 GPIO_SetBits(GPIOA, GPIO_Pin_15)
#define IO13_IN GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_15)

#define IO14_OUT_0 GPIO_ResetBits(GPIOC, GPIO_Pin_10) // PC.10
#define IO14_OUT_1 GPIO_SetBits(GPIOC, GPIO_Pin_10)
#define IO14_IN GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_10)

#define IO15_OUT_0 GPIO_ResetBits(GPIOD, GPIO_Pin_2) // PD.2
#define IO15_OUT_1 GPIO_SetBits(GPIOD, GPIO_Pin_2)
#define IO15_IN GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_2)

#define IO16_OUT_0 GPIO_ResetBits(GPIOC, GPIO_Pin_13) // PC.13
#define IO16_OUT_1 GPIO_SetBits(GPIOC, GPIO_Pin_13)
#define IO16_IN GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_13)

#define IO17_OUT_0 GPIO_ResetBits(GPIOB, GPIO_Pin_9) // PB.9
#define IO17_OUT_1 GPIO_SetBits(GPIOB, GPIO_Pin_9)
#define IO17_IN GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_9)

void toggleEfuseOn(void);
void efuseOff(void);

#ifdef __cplusplus
}
#endif
#endif /* _PINMAP_H_ */
