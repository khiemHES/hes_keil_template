#ifndef _HES_SERIALHAL_H
#define _HES_SERIALHAL_H
#ifdef __cplusplus
extern "C" {
#endif

void Setup_Test_SerialHAL(void);
void Test_Driver_Serial(void);
void Test_PutChar_cSerial(void);
void Test_Echo_cSerial(void);

#ifdef __cplusplus
}
#endif
#endif
