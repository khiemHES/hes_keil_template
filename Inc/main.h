/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define GPIO_WA_LEVEL_DET2_Pin GPIO_PIN_13
#define GPIO_WA_LEVEL_DET2_GPIO_Port GPIOC
#define ADC_PRESSURE_1_Pin GPIO_PIN_0
#define ADC_PRESSURE_1_GPIO_Port GPIOC
#define ADC_PRESSURE_2_Pin GPIO_PIN_1
#define ADC_PRESSURE_2_GPIO_Port GPIOC
#define ADC_PT1K_TEMP_1_Pin GPIO_PIN_2
#define ADC_PT1K_TEMP_1_GPIO_Port GPIOC
#define ADC_PT1K_TEMP_2_Pin GPIO_PIN_3
#define ADC_PT1K_TEMP_2_GPIO_Port GPIOC
#define ADC_EBATT_VOL_Pin GPIO_PIN_0
#define ADC_EBATT_VOL_GPIO_Port GPIOA
#define ADC_IBATT_VOL_Pin GPIO_PIN_1
#define ADC_IBATT_VOL_GPIO_Port GPIOA
#define ADC_FUELCELL_VOL_Pin GPIO_PIN_2
#define ADC_FUELCELL_VOL_GPIO_Port GPIOA
#define ADC_FUELCELL_CUR_Pin GPIO_PIN_3
#define ADC_FUELCELL_CUR_GPIO_Port GPIOA
#define PWM_STACKFAN_CTRL_Pin GPIO_PIN_6
#define PWM_STACKFAN_CTRL_GPIO_Port GPIOA
#define PWM_WA_PUMP_CTRL_Pin GPIO_PIN_7
#define PWM_WA_PUMP_CTRL_GPIO_Port GPIOA
#define USB_TX_Pin GPIO_PIN_4
#define USB_TX_GPIO_Port GPIOC
#define USB_RX_Pin GPIO_PIN_5
#define USB_RX_GPIO_Port GPIOC
#define PWM_HEATER_HS_Pin GPIO_PIN_0
#define PWM_HEATER_HS_GPIO_Port GPIOB
#define PWM_HEATER_LS_Pin GPIO_PIN_1
#define PWM_HEATER_LS_GPIO_Port GPIOB
#define SPI2_CS_LCD_Pin GPIO_PIN_12
#define SPI2_CS_LCD_GPIO_Port GPIOB
#define SPI2_SCK_LCD_Pin GPIO_PIN_13
#define SPI2_SCK_LCD_GPIO_Port GPIOB
#define SPI2_MISO_LCD_Pin GPIO_PIN_14
#define SPI2_MISO_LCD_GPIO_Port GPIOB
#define SPI2_MOSI_LCD_Pin GPIO_PIN_15
#define SPI2_MOSI_LCD_GPIO_Port GPIOB
#define GPIO_RST_LCD_Pin GPIO_PIN_6
#define GPIO_RST_LCD_GPIO_Port GPIOC
#define GPIO_D_C_LCD_Pin GPIO_PIN_7
#define GPIO_D_C_LCD_GPIO_Port GPIOC
#define GPIO_BUTTON_PRESS_Pin GPIO_PIN_8
#define GPIO_BUTTON_PRESS_GPIO_Port GPIOC
#define GPIO_PWR_KILL_Pin GPIO_PIN_9
#define GPIO_PWR_KILL_GPIO_Port GPIOC
#define PWM_VALVE1_CTRL_Pin GPIO_PIN_8
#define PWM_VALVE1_CTRL_GPIO_Port GPIOA
#define PWM_VALV2_CTRL_Pin GPIO_PIN_9
#define PWM_VALV2_CTRL_GPIO_Port GPIOA
#define PWM_VALVE3_CTRL_Pin GPIO_PIN_10
#define PWM_VALVE3_CTRL_GPIO_Port GPIOA
#define PWM_REACT_FAN_CTRL_Pin GPIO_PIN_11
#define PWM_REACT_FAN_CTRL_GPIO_Port GPIOA
#define SPI1_CS_ADS1118_Pin GPIO_PIN_15
#define SPI1_CS_ADS1118_GPIO_Port GPIOA
#define GPIO_EFUSE_ENABLE_Pin GPIO_PIN_2
#define GPIO_EFUSE_ENABLE_GPIO_Port GPIOD
#define SPI1_SCK_ADS1118_Pin GPIO_PIN_3
#define SPI1_SCK_ADS1118_GPIO_Port GPIOB
#define SPI1_MISO_ADS1118_Pin GPIO_PIN_4
#define SPI1_MISO_ADS1118_GPIO_Port GPIOB
#define SPI1_MOSI_ADS1118_Pin GPIO_PIN_5
#define SPI1_MOSI_ADS1118_GPIO_Port GPIOB
#define I2C1_SCL_EEPROM_Pin GPIO_PIN_6
#define I2C1_SCL_EEPROM_GPIO_Port GPIOB
#define I2C1_SDA_EEPROM_Pin GPIO_PIN_7
#define I2C1_SDA_EEPROM_GPIO_Port GPIOB
#define GPIO_DEBUG_LED_Pin GPIO_PIN_8
#define GPIO_DEBUG_LED_GPIO_Port GPIOB
#define GPIO_WA_LEVEL_DET1_Pin GPIO_PIN_9
#define GPIO_WA_LEVEL_DET1_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */
#define PROJECT_CODE  ("STK12W")
#define PROJECT_VERSION ("0.0.1")
// #define NDEBUG
/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
